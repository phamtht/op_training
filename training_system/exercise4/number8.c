#include<unistd.h>
#include<stdio.h>
#include<fcntl.h>
#include<stdlib.h>

int main()
{
    int fd1,fd2;
    char buf[512];
    int nbytes;
    if ((fd1 =open("file1.txt",O_RDONLY))<0)
    {
        perror("open");
        exit(1);
    }
    if((fd2 = open("file_copy.txt",O_WRONLY))<0)
    {
        perror("open");
        exit(1);
    }  
    
    while ((nbytes =read(fd1,buf,sizeof(buf)))>0)
    {
        printf("%d",nbytes);
        if((nbytes=write(fd2,buf,nbytes))<0)
        {
            exit(20);
        }
    }
    if(nbytes< 0){
        printf("Copy failed");
        exit(10);
    }
    if(close(fd1)<0){
        perror("close");
        exit(1);
    }
    if(close(fd2)<0){
        perror("close");
    }
    exit(0);
}