#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

int main()
{
    pid_t pid;
    int child_status;
    printf("\nThe process id is %d\n",getpid());
    
    pid =fork();
    if (pid <0)
    {
        //fork has failed
        printf("\nFork Failure\n");
    }
    else if (pid ==0)
    {
        //child process
        printf("\nChild process\n");
        printf("\nThe process id is %d\n",getpid());
        sleep(20);
    }
    else
    {
        //parent process
        wait(&child_status);
        printf("\nParent process\n");
        printf("\n The process id is %d\n",getpid());
        sleep(30);
    }
    
    return 0;    
}