#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

int main()
{
    
    pid_t pid =fork();
    int res;
    if(pid==0)
    {
        res= execl("/usr/bin/ls","ls",NULL);
        if (res <0)
        {
            printf("error: execl failed\n");
            exit(-1);
        }             
    }
    wait(&res);
    printf("Hello from the the process %d side!\n",getpid());
    if(WIFEXITED(res)&&(WEXITSTATUS(res)==0)){
        printf("Listing completed successfully\n");
    }
    else{
        printf(" Listing failed\n");
    }
    exit(0);
}